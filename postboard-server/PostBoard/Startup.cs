﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.IO;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Http;
using PostBoard.Controllers;
using PostBoard.Database;
using PostBoard.Database.Models;
using PostBoard.Database.Repositories;
using PostBoard.Database.Repositories.Interfaces;
using PostBoard.Services;
using PostBoard.Services.Interfaces;

namespace PostBoard
{
    public class Startup
    {
        private const string LocalHost = "http://localhost";
        private const string FrontEndPort = ":4200";

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEntityFramework()
                .AddDbContext<PostBoardContext>(opt => opt.UseSqlite("Filename=./gossip.db",
                    x => x.SuppressForeignKeyEnforcement()));

            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<PostBoardContext>();

            services.Configure<JWTSettings>(Configuration.GetSection("JWTSettings"));

            services.Configure<IdentityOptions>(opt =>
            {
                opt.Password.RequireDigit = false;
                opt.Password.RequiredLength = 5;
                opt.Password.RequireLowercase = false;
                opt.Password.RequireNonAlphanumeric = false;
                opt.Password.RequireUppercase = false;
            });

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IPostRepository, PostRepository>();
            services.AddScoped<IBoardRepository, BoardRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IPostService, PostService>();
            services.AddScoped<IBoardService, BoardService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IFileSaveService, FileSaveService>();

            // Add Swagger
            services.AddSwaggerGen(c =>
                c.SwaggerDoc("gossip_board", new Info()));

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials());
            });

            services.AddMvc();
            services.AddSignalR(x => x.Hubs.EnableDetailedErrors = true);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            //Initialise db
            var dbContext = app.ApplicationServices.GetService<PostBoardContext>();
            dbContext.Database.EnsureCreated();

            app.UseIdentity();
            app.UseCors("CorsPolicy");

            var imagesDirectoryPath = Path.Combine(Directory.GetCurrentDirectory(), "images");
            Directory.CreateDirectory(imagesDirectoryPath);

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(imagesDirectoryPath),
                RequestPath = new PathString("/images")
            });

            app.UseDeveloperExceptionPage();

            //Swagger setup
            const string swaggerUrl = "/swagger/gossip_board/swagger.json";
            app.UseSwagger()
                .UseSwaggerUI(c =>
                {
                    c.DocExpansion("none");
                    c.SwaggerEndpoint(swaggerUrl, "Gossip Board");
                });
            app.UseMvc();
            app.UseSignalR();
        }
    }
}