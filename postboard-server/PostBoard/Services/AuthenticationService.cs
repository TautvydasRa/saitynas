﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using PostBoard.Database.Models;
using PostBoard.Services.Interfaces;

namespace PostBoard.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly JWTSettings _options;

        public AuthenticationService(UserManager<User> userManager, SignInManager<User> signInManager,
            IOptions<JWTSettings> optionsAccessor)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _options = optionsAccessor.Value;
        }

        public async Task<JsonResult> Register(RegisterCredentials credentials, bool isModelStateValid)
        {
            if (!isModelStateValid) return Error("Unexpected error");
            var user = new User(credentials.Username);
            var result = await _userManager.CreateAsync(user, credentials.Password);
            if (!result.Succeeded) return Errors(result);
            await _signInManager.SignInAsync(user, false);
            return new JsonResult(new Dictionary<string, object>
            {
                {"access_token", GetAccessToken(credentials.Username)},
                {"id_token", GetIdToken(user)}
            });
        }

        public async Task<JsonResult> SignIn(SignInCredentials credentials, bool isModelStateValid)
        {
            if (!isModelStateValid) return Error("Unexpected error");
            var result =
                await _signInManager.PasswordSignInAsync(credentials.Username, credentials.Password, false, false);
            if (!result.Succeeded) return new JsonResult("Unable to sign in") {StatusCode = 401};
            var user = await _userManager.FindByEmailAsync(credentials.Username);
            return new JsonResult(new Dictionary<string, object>
            {
                {"access_token", GetAccessToken(credentials.Username)},
                {"id_token", GetIdToken(user)}
            });
        }

        private static JsonResult Error(string error)
        {
            return new JsonResult(error) {StatusCode = 400};
        }

        private static JsonResult Errors(IdentityResult result)
        {
            var items = result.Errors
                .Select(x => x.Description)
                .ToArray();
            return new JsonResult(items) {StatusCode = 400};
        }

        private string GetAccessToken(string username)
        {
            var payload = new Dictionary<string, object>
            {
                {"sub", username},
                {"username", username}
            };
            return GetToken(payload);
        }

        private string GetIdToken(User user)
        {
            var payload = new Dictionary<string, object>
            {
                {"id", user.Id},
                {"sub", user.UserName},
                {"username", user.UserName}
            };
            return GetToken(payload);
        }

        private string GetToken(IDictionary<string, object> payload)
        {
            var secretKey = _options.SecretKey;
            payload.Add("iss", _options.Issuer);
            payload.Add("aud", _options.Audience);
            payload.Add("nbf", ConvertToUnixTimestamp(DateTime.Now));
            payload.Add("iat", ConvertToUnixTimestamp(DateTime.Now));
            payload.Add("exp", ConvertToUnixTimestamp(DateTime.Now.AddDays(7)));
            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);
            return encoder.Encode(payload, secretKey);
        }

        private static double ConvertToUnixTimestamp(DateTime date)
        {
            var origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var difference = date.ToUniversalTime() - origin;
            return Math.Floor(difference.TotalSeconds);
        }
    }
}