﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PostBoard.Database;
using PostBoard.Database.Models;
using PostBoard.Services.Interfaces;

namespace PostBoard.Services
{
    public class PostService : IPostService
    {
        private readonly IUnitOfWork _unitOfWork;

        public PostService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Post> AddPost(Post post)
        {
            post.CreatedAt = DateTime.Now;
            post.Reactions = new List<Reaction>();
            await _unitOfWork.PostRepository.AddPost(post);
            await _unitOfWork.SaveChanges();
            return await _unitOfWork.PostRepository.GetPostById(post.Id);
        }

        public Task<Post> GetPost(int id)
        {
            return _unitOfWork.PostRepository.GetPostById(id);
        }

        public Task<List<Post>> GetPosts(int limit)
        {
            return _unitOfWork.PostRepository.GetPosts(limit);
        }

        public async Task UpdatePost(Post post)
        {
            _unitOfWork.PostRepository.EditPost(post);
            await _unitOfWork.SaveChanges();
        }

        public async Task RemovePost(int id)
        {
            _unitOfWork.PostRepository.RemovePost(id);
            await _unitOfWork.SaveChanges();
        }

        public async Task React(Reaction reaction, int postId)
        {
            await _unitOfWork.PostRepository.React(reaction, postId);
            await _unitOfWork.SaveChanges();
        }

        public async Task RemoveReaction(string username, int postId)
        {
            await _unitOfWork.PostRepository.RemoveReaction(username, postId);
            await _unitOfWork.SaveChanges();
        }
    }
}