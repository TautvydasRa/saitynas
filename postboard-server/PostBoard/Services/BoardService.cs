﻿using PostBoard.Database;
using PostBoard.Database.Models;
using PostBoard.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostBoard.Services
{
    public class BoardService : IBoardService
    {
        private readonly IUnitOfWork _unitOfWork;

        public BoardService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Board> AddBoard(Board board)
        {
            if (board.Members == null) board.Members = new List<User>();
            await _unitOfWork.BoardRepository.AddBoard(board);
            await _unitOfWork.SaveChanges();
            return await _unitOfWork.BoardRepository.GetBoard(board.Id);
        }

        public async Task<Board> EditBoard(Board board)
        {
            _unitOfWork.BoardRepository.EditBoard(board);
            await _unitOfWork.SaveChanges();
            return board;
        }

        public async Task RemoveBoard(int boardId)
        {
            _unitOfWork.BoardRepository.RemoveBoard(boardId);
            await _unitOfWork.SaveChanges();
        }

        public Task<Board> GetBoard(int boardId)
        {
            return _unitOfWork.BoardRepository.GetBoard(boardId);
        }

        public Task<List<Board>> GetBoards(int limit)
        {
            return _unitOfWork.BoardRepository.GetBoards(limit);
        }

        public async Task<Board> AddPost(Post post, int boardId)
        {
            var board = await _unitOfWork.BoardRepository.GetBoard(boardId);
            if (board == null) return null;
            post.CreatedAt = DateTime.Now;
            if (post.Reactions == null) post.Reactions = new List<Reaction>();
            _unitOfWork.BoardRepository.EditBoard(board);
            await _unitOfWork.SaveChanges();
            return board;
        }

        public async Task<Board> EditPost(Post post, int boardId)
        {
            var board = await _unitOfWork.BoardRepository.GetBoard(boardId);
            if (board == null) return null;
            _unitOfWork.BoardRepository.EditBoard(board);
            await _unitOfWork.SaveChanges();
            return board;
        }

        public async Task<Board> RemovePost(Post post, int boardId)
        {
            var board = await _unitOfWork.BoardRepository.GetBoard(boardId);
            if (board == null) return null;
            _unitOfWork.BoardRepository.EditBoard(board);
            await _unitOfWork.SaveChanges();
            return board;
        }
    }
}