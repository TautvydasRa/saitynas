﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PostBoard.Services.Interfaces
{
    public interface IFileSaveService
    {
        Task<FileSaveService.FileUploadResultDto> Upload(IFormFile file);
    }
}