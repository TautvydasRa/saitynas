﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PostBoard.Database.Models;

namespace PostBoard.Services.Interfaces
{
    public interface IAuthenticationService
    {
        Task<JsonResult> Register(RegisterCredentials credentials, bool isModelStateValid);
        Task<JsonResult> SignIn(SignInCredentials credentials, bool isModelStateValid);
    }
}