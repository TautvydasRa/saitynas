﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PostBoard.Database.Models;

namespace PostBoard.Services.Interfaces
{
    public interface IPostService
    {
        Task<Post> AddPost(Post post);
        Task<Post> GetPost(int id);
        Task<List<Post>> GetPosts(int limit);
        Task UpdatePost(Post post);
        Task RemovePost(int id);
        Task React(Reaction reaction, int postId);
        Task RemoveReaction(string username, int postId);
    }
}
