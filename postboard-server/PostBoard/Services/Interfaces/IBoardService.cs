﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PostBoard.Database.Models;

namespace PostBoard.Services.Interfaces
{
    public interface IBoardService
    {
        Task<Board> AddBoard(Board board);
        Task<Board> EditBoard(Board board);
        Task RemoveBoard(int boardId);
        Task<Board> GetBoard(int boardId);
        Task<List<Board>> GetBoards(int limit);
    }
}
