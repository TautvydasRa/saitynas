﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PostBoard.Database.Models;

namespace PostBoard.Services.Interfaces
{
    public interface IUserService
    {
        Task<User> GetUserByUsename(string username);
        Task<User> GetUserById(string userId);
    }
}