﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Linq;
using System;
using PostBoard.Database.Models;
using PostBoard.Services.Interfaces;

namespace PostBoard.Controllers
{
    [EnableCors("CorsPolicy")]
    [Route("api/post")]
    public class PostController : Controller
    {
        private readonly IPostService _postService;

        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        [HttpGet]
        [Produces(typeof(Post[]))]
        public async Task<IActionResult> GetAllPosts([FromQuery] int limit = 10)
        {
            var posts = await _postService.GetPosts(limit);
            return Ok(posts);
        }

        [HttpGet("{id}")]
        [Produces(typeof(Post[]))]
        public async Task<IActionResult> GetPost([FromRoute] int id)
        {
            var result = await _postService.GetPost(id);
            if (result == null)
                return NotFound("Not found");
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddPost([FromBody] Post post)
        {
            var newPost = await _postService.AddPost(post);
            if (newPost != null)
            {
                return Ok(newPost);
            }
            return BadRequest("Something went wrong");
        }

        [HttpPut("react/{postId}")]
        public async Task<IActionResult> React([FromBody] Reaction reaction, int postId)
        {
            try
            {
                await _postService.React(reaction, postId);
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message) {StatusCode = 404};
            }
            return Ok();
        }

        [HttpDelete("react/{username},{postId}")]
        public async Task<IActionResult> RemoveReaction(string username, int postId)
        {
            try
            {
                await _postService.RemoveReaction(username, postId);
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message) {StatusCode = 404};
            }
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> EditPost([FromBody] Post post)
        {
            if (post == null) return BadRequest("Something went wrong");
            await _postService.UpdatePost(post);
            var result = await _postService.GetPost(post.Id);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost(int id)
        {
            await _postService.RemovePost(id);
            return Ok();
        }
    }
}