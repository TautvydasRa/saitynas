﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PostBoard.Services;
using PostBoard.Services.Interfaces;
using static PostBoard.Services.FileSaveService;

namespace PostBoard.Controllers
{
    [Route("api/fileUpload")]
    public class FileUploadController : Controller
    {
        private readonly IFileSaveService _fileSaveService;

        public FileUploadController(IFileSaveService fileService)
        {
            _fileSaveService = fileService;
        }

        [HttpPost]
        [Produces(typeof(FileUploadResultDto))]
        public async Task<IActionResult> Upload(List<IFormFile> files)
        {
            if (files.Count == 0)
            {
                return BadRequest("Missing file. Are you missing form-data file key?");
            }
            var result = await _fileSaveService.Upload(files.FirstOrDefault());
            return Ok(result);
        }
    }
}