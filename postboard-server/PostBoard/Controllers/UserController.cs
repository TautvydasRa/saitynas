﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PostBoard.Database.Models;
using PostBoard.Services.Interfaces;

namespace PostBoard.Controllers
{
    [Route("api/users")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("getUserByUsername")]
        [Produces(typeof(User))]
        public async Task<IActionResult> GetUserByUsername(string username)
        {
            var result = await _userService.GetUserByUsename(username);
            return Ok(result);
        }

        [HttpGet("getUserById")]
        [Produces(typeof(User))]
        public async Task<IActionResult> GetUserById(string userId)
        {
            var result = await _userService.GetUserById(userId);
            return Ok(result);
        }
    }
}