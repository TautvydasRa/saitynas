﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using PostBoard.Database.Models;
using PostBoard.Services.Interfaces;

namespace PostBoard.Controllers
{
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly IAuthenticationService _authenticationService;

        public AccountController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost("sign-in")]
        public async Task<IActionResult> SignIn([FromBody] SignInCredentials credentials)
        {
            var result = await _authenticationService.SignIn(credentials, ModelState.IsValid);
            return Ok(result);
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterCredentials credentials)
        {
            var result = await _authenticationService.Register(credentials, ModelState.IsValid);
            return Ok(result);
        }
    }
}
