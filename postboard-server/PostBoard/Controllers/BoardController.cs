﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Linq;
using System;
using PostBoard.Database.Models;
using PostBoard.Services.Interfaces;

namespace PostBoard.Controllers
{
    [EnableCors("CorsPolicy")]
    [Route("api/board")]
    public class BoardController : Controller
    {
        private readonly IBoardService _boardService;

        public BoardController(IBoardService boardService)
        {
            this._boardService = boardService;
        }

        [HttpPost]
        public async Task<IActionResult> AddBoard([FromBody] Board board)
        {
            var newBoard = await _boardService.AddBoard(board);
            if (newBoard != null)
            {
                return Ok(newBoard);
            }
            return BadRequest("Something went wrong");
        }

        [HttpPut]
        public async Task<IActionResult> EditBoard([FromBody] Board board)
        {
            if (board == null) return BadRequest("Something went wrong");
            await _boardService.EditBoard(board);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBoard(int id)
        {
            await _boardService.RemoveBoard(id);
            return Ok();
        }

        [HttpGet("{id}")]
        [Produces(typeof(Board))]
        public async Task<IActionResult> GetBoard([FromRoute] int id)
        {
            var board = await _boardService.GetBoard(id);
            if (board == null) return NotFound("Not found");
            return Ok(board);
        }

        [HttpGet]
        [Produces(typeof(Board[]))]
        public async Task<IActionResult> GetBoards([FromQuery] int limit = 10)
        {
            var boards = await _boardService.GetBoards(limit);
            return Ok(boards);
        }
    }
}