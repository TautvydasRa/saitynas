﻿using System.Threading.Tasks;
using PostBoard.Database.Repositories.Interfaces;

namespace PostBoard.Database
{
    public class UnitOfWork : IUnitOfWork
    {
        protected readonly PostBoardContext Context;

        public IPostRepository PostRepository { get; }
        public IUserRepository UserRepository { get; }
        public IBoardRepository BoardRepository { get; }

        public UnitOfWork(PostBoardContext context, IPostRepository postRepository, IUserRepository userRepository, IBoardRepository boardRepository)
        {
            Context = context;
            PostRepository = postRepository;
            UserRepository = userRepository;
            BoardRepository = boardRepository;
        }

        public Task<int> SaveChanges()
        {
            return Context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Context.Dispose();
        }

    }
}
