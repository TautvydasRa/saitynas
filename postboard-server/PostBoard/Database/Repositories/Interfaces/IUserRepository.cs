﻿using System.Threading.Tasks;
using PostBoard.Database.Models;

namespace PostBoard.Database.Repositories.Interfaces
{
    public interface IUserRepository
    {
        Task<User> GetUserByUsername(string username);
    }
}
