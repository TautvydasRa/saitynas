﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PostBoard.Database.Models;

namespace PostBoard.Database.Repositories.Interfaces
{
    public interface IPostRepository
    {
        Task<Post> GetPostById(int id);
        Task AddPost(Post post);
        Task<List<Post>> GetPosts(int limit);
        void EditPost(Post post);
        void RemovePost(int id);
        Task React(Reaction reaction, int postId);
        Task RemoveReaction(string username, int postId);
    }
}
