﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PostBoard.Database.Models;

namespace PostBoard.Database.Repositories.Interfaces
{
    public interface IBoardRepository
    {
        Task AddBoard(Board board);
        void EditBoard(Board board);
        void RemoveBoard(int boardId);
        Task<Board> GetBoard(int boardId);
        Task<List<Board>> GetBoards(int limit);
    }
}
