﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PostBoard.Database.Models;
using PostBoard.Database.Repositories.Interfaces;

namespace PostBoard.Database.Repositories
{
    public class PostRepository : IPostRepository
    {
        protected readonly PostBoardContext Context;

        public PostRepository(PostBoardContext context)
        {
            Context = context;
        }

        public async Task<Post> GetPostById(int id)
        {
            return await Context.Posts
                .Include(post => post.Reactions)
                .Include(post => post.Comments)
                .Where(post => post.Id == id)
                .FirstOrDefaultAsync();
        }

        public async Task AddPost(Post post)
        {
            await Context.Posts.AddAsync(post);
            await Context.SaveChangesAsync();
        }

        public async Task<List<Post>> GetPosts(int limit)
        {
            return await Context.Posts
                .Include(post => post.Reactions)
                .Include(post => post.Comments)
                .OrderByDescending(p => p.IsPinned)
                .ThenByDescending(p => p.CreatedAt)
                .Take(limit)
                .ToListAsync();
        }

        public void EditPost(Post post)
        {
            Context.Posts.Update(post);
        }

        public void RemovePost(int id)
        {
            var post = Context.Posts.FirstOrDefault(x => x.Id == id);
            if (post != null)
            {
                Context.Posts.Remove(post);
            }
        }

        public async Task React(Reaction reaction, int postId)
        {
            var post = await Context.Posts.Include(p => p.Reactions).Where(p => p.Id == postId).FirstOrDefaultAsync();
            if (post != null)
            {
                post.Reactions.RemoveAll(r => r.Author == reaction.Author);
                post.Reactions.Add(reaction);
            }
        }

        public async Task RemoveReaction(string username, int postId)
        {
            var post = await Context.Posts.Include(p => p.Reactions).Where(p => p.Id == postId).FirstOrDefaultAsync();
            post?.Reactions.RemoveAll(p => p.Author == username);
        }
    }
}