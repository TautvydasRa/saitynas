﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PostBoard.Database.Models;
using PostBoard.Database.Repositories.Interfaces;

namespace PostBoard.Database.Repositories
{
    public class BoardRepository : IBoardRepository
    {
        protected readonly PostBoardContext Context;

        public BoardRepository(PostBoardContext context)
        {
            Context = context;
        }

        public async Task AddBoard(Board board)
        {
            await Context.Boards.AddAsync(board);
            await Context.SaveChangesAsync();
        }

        public void EditBoard(Board board)
        {
            Context.Boards.Update(board);
        }

        public void RemoveBoard(int boardId)
        {
            var board = Context.Boards.FirstOrDefault(b => b.Id == boardId);
            if (board != null)
            {
                Context.Boards.Remove(board);
            }
        }

        public async Task<Board> GetBoard(int boardId)
        {
            return await Context.Boards
                .Include(b => b.Members)
                .Where(b => b.Id == boardId)
                .FirstOrDefaultAsync();
        }

        public async Task<List<Board>> GetBoards(int limit)
        {
            return await Context.Boards
                .Include(b => b.Members)
                .OrderBy(b => b.Name)
                .Take(limit)
                .ToListAsync();
        }
    }
}