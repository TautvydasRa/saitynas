﻿using System.Threading.Tasks;
using PostBoard.Database.Models;
using PostBoard.Database.Repositories.Interfaces;

namespace PostBoard.Database.Repositories
{
    public class UserRepository : IUserRepository
    {
        protected readonly PostBoardContext Context;

        public UserRepository(PostBoardContext context)
        {
            Context = context;
        }

        public async Task<User> GetUserByUsername(string username)
        {
            return await Context.Users.FindAsync(username);
        }
    }
}