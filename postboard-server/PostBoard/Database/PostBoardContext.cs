﻿using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using PostBoard.Database.Models;

namespace PostBoard.Database
{
    public class PostBoardContext : IdentityDbContext<User>
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Board> Boards { get; set; }

        public PostBoardContext(DbContextOptions options) : base(options)
        {

        }
    }
}