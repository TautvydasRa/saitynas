﻿using System;
using System.Threading.Tasks;
using PostBoard.Database.Repositories.Interfaces;

namespace PostBoard.Database
{
    public interface IUnitOfWork : IDisposable
    {
        IPostRepository PostRepository { get; }
        IUserRepository UserRepository { get; }
        IBoardRepository BoardRepository { get; }
        Task<int> SaveChanges();
    }
}
