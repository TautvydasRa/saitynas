﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PostBoard.Database.Models.Enumerations;

namespace PostBoard.Database.Models
{
    public class Post
    {
        [Key]
        public int Id { get; set; }

        public string AuthorId { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public DateTime CreatedAt { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public List<Reaction> Reactions { get; set; }
        public bool IsPinned { get; set; }
        public string Color { get; set; }
        public string Board { get; set; }
    }
}