﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using PostBoard.Database.Models.Enumerations;

namespace PostBoard.Database.Models
{
    public class Reaction
    {
        [Key]
        public int Id { get; set; }

        public string Author { get; set; }
        public ReactionType ReactionType { get; set; }

        public override bool Equals(object obj)
        {
            var item = obj as Reaction;
            return item != null && item.Author == Author;
        }

        public override int GetHashCode()
        {
            return ReactionType.GetHashCode();
        }
    }
}