﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace PostBoard.Database.Models
{
    public sealed class User : IdentityUser
    {
        public User()
        {
            
        }

        public User(string username)
        {
            UserName = username;
        }
    }
}