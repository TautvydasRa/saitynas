﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostBoard.Database.Models.Enumerations
{
    public enum ReactionType
    {
        Like,
        Dislike
    }
}
