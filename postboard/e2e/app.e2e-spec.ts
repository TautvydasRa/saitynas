import { PostBoardPage } from './app.po';

describe('postboard App', () => {
  let page: PostBoardPage;

  beforeEach(() => {
    page = new PostBoardPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
