import {Component} from '@angular/core';
import {UIService} from './services/ui.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor() {
  }

  isSignedIn() {
    return !!localStorage.getItem('currentUser');
  }
}
