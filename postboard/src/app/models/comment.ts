export class Comment {
  id: number;
  content: string;
  author: string;
  createdAt: Date;
}
