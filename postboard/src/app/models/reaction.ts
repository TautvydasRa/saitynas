import {ReactionType} from './reaction-type.enum';

export class Reaction {
  id: number;
  author: string;
  reactionType: ReactionType;
}
