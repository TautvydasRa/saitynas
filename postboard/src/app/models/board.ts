import {User} from './user';
import {Post} from './post';

export class Board {
  id: number;
  name: string;
  author: string;
  members: User[];
}
