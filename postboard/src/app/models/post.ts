import {Reaction} from './reaction';
import {Comment} from './comment';

export class Post {
  id: number;
  authorId: string;
  title: string;
  link: string;
  createdAt: Date;
  comments: Comment[];
  reactions: Reaction[];
  isPinned: boolean;
  color: string;
  board: string;
}
