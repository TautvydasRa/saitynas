import {Component, OnInit, ViewChild} from '@angular/core';
import {PostService} from '../../services/post.service';
import {Post} from '../../models/post';
import {MdSidenav, MdSnackBar} from '@angular/material';
import {UIService} from '../../services/ui.service';
import {Board} from '../../models/board';
import {BoardListComponent} from '../board-list/board-list.component';
import {BoardService} from '../../services/board.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
  private static readonly ADD_POST_SIDE_NAV_ID = 0;
  private static readonly BOARDS_SIDE_NAV_ID = 1;
  private static readonly POST_COUNT = 1000;
  @ViewChild('sideNav')
  public sideNav: MdSidenav;
  public showedPosts: Post[] = [];
  public filterColor: string;
  public sideNavId: number;
  private posts: Post[] = [];
  private board: Board = null;

  constructor(private postService: PostService, private mdSnackBar: MdSnackBar, private uiService: UIService) {
    this.subscribeToAddPostSideNav(uiService);
    this.subscribeToBoardSideNav(uiService);
    this.subscribeToColorFilter(uiService);
  }

  ngOnInit() {
    this.postService.getPosts(PostListComponent.POST_COUNT).subscribe((posts: Post[]) => {
      this.posts = posts;
      this.filterAndSort();
    });
  }

  removePost(post: Post) {
    this.posts = this.posts.filter(p => p.id !== post.id);
    this.postService.deletePost(post.id).subscribe();
    this.filterAndSort();
  }

  pinPost(post: Post) {
    this.postService.updatePost(post).subscribe((pinnedPost) => {
      this.posts.find(p => p.id === post.id).isPinned = post.isPinned;
      this.filterAndSort();
    });
  }

  addPost(post: Post) {
    post.board = this.board ? this.board.name : null;
    this.postService.addPost(post).subscribe(p => {
      this.posts.unshift(p);
      this.filterAndSort();
      this.sideNav.close();
    });
  }

  openBoard(board: Board) {
    this.board = board;
    this.filterAndSort();
    this.sideNav.close();
  }

  private subscribeToAddPostSideNav(uiService: UIService) {
    uiService.addPostSideNavEvent().subscribe(() => {
      if (this.sideNavId === PostListComponent.BOARDS_SIDE_NAV_ID) {
        this.sideNav.close();
      }
      this.sideNavId = 0;
      this.toggleSideNav();
    });
  }

  private subscribeToBoardSideNav(uiService: UIService) {
    uiService.boardsSideNavEvent().subscribe(() => {
      if (this.sideNavId === PostListComponent.ADD_POST_SIDE_NAV_ID) {
        this.sideNav.close();
      }
      this.sideNavId = 1;
      this.toggleSideNav();
    });
  }

  private subscribeToColorFilter(uiService: UIService) {
    uiService.colorFilterEvent().subscribe(color => {
      this.filterColor = color;
      this.filterAndSort();
    });
  }

  private toggleSideNav() {
    return this.sideNav.toggle();
  }

  private filterAndSort() {
    this.filterBySelectedColor();
    this.filterByBoard();
    this.sortPosts();
    this.refreshView();
  }

  private filterBySelectedColor() {
    this.showedPosts = this.filterColor ? this.posts.filter(p => p.color === this.filterColor) : this.posts;
  }

  private filterByBoard() {
    this.showedPosts = this.showedPosts.filter(p => this.isBoardPost(p));
  }

  private isBoardPost(p) {
    return !p.board && !this.board || p.board && this.board && p.board === this.board.name;
  }

  private sortPosts() {
    this.showedPosts.sort((postOne, postTwo) => {
      if (postOne.isPinned === postTwo.isPinned) {
        if (postOne.createdAt > postTwo.createdAt) {
          return -1;
        }
        if (postOne.createdAt < postTwo.createdAt) {
          return 1;
        }
        return 0;
      }
      if (postOne.isPinned) {
        return -1;
      }
      if (postTwo.isPinned) {
        return 1;
      }
      return 0;
    });
  }

  private refreshView() {
    this.mdSnackBar.open('');
    this.mdSnackBar.dismiss();
  }
}
