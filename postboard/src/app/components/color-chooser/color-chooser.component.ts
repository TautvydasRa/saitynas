import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MdButtonToggleGroup} from '@angular/material';
import {UIService} from '../../services/ui.service';

@Component({
  selector: 'app-color-chooser',
  templateUrl: './color-chooser.component.html',
  styleUrls: ['./color-chooser.component.css']
})
export class ColorChooserComponent implements OnInit {
  @Output() colorSelect = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit() {
  }

  private selectColor(color: MdButtonToggleGroup) {
    this.colorSelect.emit(color.value === 'null' ? null : color.value);
  }
}
