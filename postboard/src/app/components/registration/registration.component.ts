import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {User} from '../../models/user';
import {MdDialog} from '@angular/material';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  user: User;
  loading = false;
  error = '';

  constructor(private authenticationService: AuthenticationService, private mdDialog: MdDialog) {
    this.user = new User();
  }

  ngOnInit() {
  }

  register() {
    this.loading = true;
    this.authenticationService.register(this.user.username, this.user.password).subscribe(result => {
      if (result) {
        this.mdDialog.closeAll();
      } else {
        this.error = 'This username is invalid or already taken';
        this.loading = false;
      }
    });
  }
}
