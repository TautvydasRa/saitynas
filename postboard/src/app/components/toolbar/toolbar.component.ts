import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {AuthenticationService} from '../../services/authentication.service';
import {UIService} from '../../services/ui.service';
import {Router, RouterModule} from '@angular/router';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  username: string;

  constructor(private authService: AuthenticationService, private userService: UserService, private uiService: UIService) {
  }

  ngOnInit() {
    const username = JSON.parse(localStorage.getItem('currentUser')).username;
    this.userService.getUserByUsername(username).subscribe(
      user => {
        this.username = user.username;
        localStorage.setItem('currentUserInfo', JSON.stringify({username: user.username}));
      },
      error => console.log(error));
  }

  refresh() {
    window.location.reload();
  }

  newPost() {
    this.uiService.invokeAddPostSideNav();
  }

  boards() {
    this.uiService.invokeBoardsSideNav();
  }

  filterByColor(color) {
    this.uiService.invokeColorFilter(color);
  }

  logout() {
    this.authService.logout();
    this.username = null;
  }
}
