import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Post} from '../../models/post';
import {PostService} from '../../services/post.service';
import {FileUploadService} from '../../services/file-upload.service';
import {HttpEventType, HttpResponse} from '@angular/common/http';
import {FileUploadResult} from '../../models/file-upload-result';

@Component({
  selector: 'app-post-add',
  templateUrl: './post-add.component.html',
  styleUrls: ['./post-add.component.css']
})
export class PostAddComponent implements OnInit {
  @Output() postClick = new EventEmitter<Post>();
  post: Post;
  isLinkAdded: boolean;
  imageFile: File;

  constructor(private postService: PostService, private fileUploadService: FileUploadService) {
    this.post = new Post;
    this.isLinkAdded = false;
  }

  private static removeOldImage(container: HTMLElement, image: HTMLElement) {
    if (image) {
      container.style.visibility = 'hidden';
      container.removeChild(image);
    }
  }

  ngOnInit() {
  }

  private onPostClick(form: NgForm) {
    if (this.imageFile) {
      this.uploadImage(() => this.postPost(form));
    } else {
      this.postPost(form);
    }
  }

  private onLinkChange() {
    const previewDiv = document.getElementById('preview');
    const previewImg = document.getElementById('preview-image');
    PostAddComponent.removeOldImage(previewDiv, previewImg);
    this.loadNewImage(previewDiv);
  }

  private onFileChange(event) {
    const files = event.srcElement.files;
    if (files != null) {
      this.imageFile = files[0];
      this.post.link = this.imageFile.name;
      const linkInput = document.getElementById('link') as any;
      linkInput.value = this.imageFile.name;
      linkInput.readOnly = true;
      console.log(this.imageFile);
    }
  }

  private selectColor(selectedColor: string) {
    this.post.color = selectedColor;
  }

  private postPost(form: NgForm) {
    const newPost = this.createPost();
    this.postClick.emit(newPost);
    this.reset(form);
  }

  private uploadImage(onComplete: any) {
    this.fileUploadService.uploadFile(this.imageFile).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        const percentDone = Math.round(100 * event.loaded / event.total);
        console.log(`File is ${percentDone}% uploaded.`);
      } else if (event instanceof HttpResponse) {
        if (event.ok) {
          console.log('File is completely uploaded!');
          console.log(event.body);
          const uploadResult = event.body as FileUploadResult;
          this.post.link = uploadResult.fileName;
          console.log('FILE', uploadResult.fileName);
          onComplete();
        } else {
          console.log('File upload failed!');
        }
      }
    });
  }

  private createPost() {
    const newPost = new Post();
    newPost.reactions = [];
    newPost.title = this.post.title;
    if (this.isLinkAdded) {
      newPost.link = this.imageFile ? FileUploadService.getImageUrl(this.post.link) : this.post.link;
    }
    newPost.authorId = JSON.parse(localStorage.getItem('currentUser')).username;
    newPost.color = this.post.color;
    newPost.reactions = [];
    return newPost;
  }

  private reset(form: NgForm) {
    this.post = new Post;
    this.imageFile = null;
    this.isLinkAdded = false;
    this.onLinkChange();
    form.reset();
  }

  private loadNewImage(container: HTMLElement) {
    if (this.post.link && this.isLinkAdded === true) {
      const image = new Image();
      image.id = 'preview-image';
      image.width = 350;
      image.height = 200;
      image.onload = () => {
        container.style.visibility = 'visible';
        container.appendChild(image);
      };
      image.src = this.post.link;
    }
  }
}
