import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MdDialog, MdDialogConfig} from '@angular/material';
import {LoginComponent} from '../login/login.component';
import {RegistrationComponent} from '../registration/registration.component';

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.css']
})
export class MainViewComponent implements OnInit {
  constructor(private dialog: MdDialog, private dialogConfig: MdDialogConfig) {
    dialogConfig.width = '60%';
    dialogConfig.height = '60%';
  }

  ngOnInit() {
  }

  login() {
    this.dialog.open(LoginComponent);
  }

  register() {
    this.dialog.open(RegistrationComponent);
  }
}
