import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Post} from '../../models/post';
import {UserService} from '../../services/user.service';
import {Reaction} from '../../models/reaction';
import {PostService} from '../../services/post.service';
import {User} from '../../models/user';
import {ReactionType} from '../../models/reaction-type.enum';

@Component({
  selector: 'app-small-post',
  templateUrl: './small-post.component.html',
  styleUrls: ['./small-post.component.css']
})
export class SmallPostComponent implements OnInit {
  private static readonly DEFAULT_USER_NAME = 'Anonymous';
  private static readonly MAX_RATING = 1000;
  @Input() post: Post;
  @Output() pinPost;
  @Output() removePost;
  @Output() onCommentClick;
  user: User;
  rating: number;

  constructor(private userService: UserService, private postService: PostService) {
    this.pinPost = new EventEmitter<Post>();
    this.removePost = new EventEmitter<Post>();
    this.onCommentClick = new EventEmitter();
    this.user = new User();
    this.rating = 0;
  }

  ngOnInit() {
    if (this.post.comments == null) {
      this.post.comments = [];
    }
    this.initUser();
    this.initEmotions();
  }

  comment() {
    this.onCommentClick.emit();
  }

  private initUser() {
    if (this.post.authorId != null) {
      this.userService.getUserByUsername(this.post.authorId).subscribe(
        user => {
          this.user.username = user.userName;
        },
        error => console.log(error)
      );
    } else {
      this.user.username = SmallPostComponent.DEFAULT_USER_NAME;
    }
  }

  private initEmotions() {
    for (const reaction of this.post.reactions) {
      switch (reaction.reactionType) {
        case ReactionType.LIKE:
          this.rating++;
          break;
        case ReactionType.DISLIKE:
          this.rating--;
          break;
      }
    }
  }

  private openPostView() {
    // this.dialogConfig.height = '90%';
    // this.dialogConfig.width = '80%';
    // console.log('post view opening');
    // const dialogRef = this.dialog.open(PostViewComponent, this.dialogConfig);
    // dialogRef.componentInstance.post = this.post;
  }

  private pin() {
    this.post.isPinned = !this.post.isPinned;
    this.pinPost.emit(this.post);
  }

  private isImage() {
    return (this.post.link.match(/\.(jpeg|jpg|gif|png)$/) != null);
  }

  private deletePost() {
    const isDeletionConfirmed = confirm('Do you really want to delete this post. You will not be able to revert this action');
    if (isDeletionConfirmed) {
      this.removePost.emit(this.post);
    }
  }

  // todo
  private like() {
    const userReaction = this.getUserReaction();
    if (userReaction) {
      if (userReaction.reactionType === ReactionType.LIKE) {
        return;
      }
      this.rating++;
      this.post.reactions = this.post.reactions.filter(r => r.author !== this.user.username);
    }
    this.rating++;
    const reaction = this.createReaction(ReactionType.LIKE);
    this.postService.react(reaction, this.post.id).subscribe();
    this.post.reactions.push(reaction);
  }

  private dislike() {
    const userReaction = this.getUserReaction();
    if (userReaction) {
      if (userReaction.reactionType === ReactionType.DISLIKE) {
        return;
      }
      this.rating--;
      this.postService.removeReaction(this.user.username, this.post.id).subscribe();
      this.post.reactions = this.post.reactions.filter(r => r.author !== this.user.username);
    }
    this.rating--;
    const reaction = this.createReaction(ReactionType.DISLIKE);
    this.postService.react(reaction, this.post.id).subscribe();
    this.post.reactions.push(reaction);
  }

  private getUserReaction() {
    return this.post.reactions.find(p => p.author === this.user.username);
  }

  private createReaction(reactionType: ReactionType) {
    const reaction = new Reaction();
    reaction.author = this.user.username;
    reaction.reactionType = reactionType;
    return reaction;
  }

  private isLiked() {
    return this.isReacted(ReactionType.LIKE);
  }

  private isDisliked() {
    return this.isReacted(ReactionType.DISLIKE);
  }

  private isReacted(reactionType: ReactionType) {
    return this.post.reactions.some(r => r.author === this.user.username && r.reactionType === reactionType);
  }

  private getRating() {
    if (this.rating >= SmallPostComponent.MAX_RATING) {
      return '999+';
    }
    if (this.rating <= SmallPostComponent.MAX_RATING * -1) {
      return '-999';
    }
    return this.rating;
  }
}
