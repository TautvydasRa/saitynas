import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {MdDialog} from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: any = {};
  loading = false;
  error = '';

  constructor(private authService: AuthenticationService, private mdDialog: MdDialog) {
  }

  ngOnInit() {
  }

  login() {
    this.loading = true;
    this.authService.login(this.user.username, this.user.password).subscribe(result => {
      if (result) {
        this.mdDialog.closeAll();
      } else {
        this.error = 'Username or password is incorrect';
        this.loading = false;
      }
    });

  }
}
