import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BoardService} from '../../services/board.service';
import {Board} from '../../models/board';

@Component({
  selector: 'app-board-list',
  templateUrl: './board-list.component.html',
  styleUrls: ['./board-list.component.css']
})
export class BoardListComponent implements OnInit {
  @Output() openBoardClick = new EventEmitter<Board>();
  public boards: Board[];

  constructor(private boardService: BoardService) {
    this.boards = [];
  }

  ngOnInit() {
    this.boardService.getBoards(100).subscribe((boards: Board[]) => {
      console.log(boards);
      this.boards = boards;
    });
  }

  openMainBoard() {
    this.openBoard(null);
  }

  openBoard(board: Board) {
    this.openBoardClick.emit(board);
  }

  createNewBoard(boardNameInput) {
    const board = new Board();
    board.name = boardNameInput.value;
    if (boardNameInput.value) {
      boardNameInput.value = '';
      this.boardService.addBoard(board).subscribe((newBoard: Board) => {
        this.boards.unshift(newBoard);
      });
    }
  }

  deleteBoard(board: Board) {
    if (confirm('Do you really want to delete selected board?')) {
      this.boardService.removeBoard(board.id).subscribe(() => {
          this.boards = this.boards.filter(b => b.id !== board.id);
        }
      );
    }
  }
}
