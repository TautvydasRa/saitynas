import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';

@Injectable()
export class RouteGuard implements CanActivate {
  constructor(private router: Router) {
  }

  canActivate() {
    if (!localStorage.getItem('currentUser')) {
      return true;
    }
    this.router.navigate(['/post_list']);
    return false;
  }
}
