import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {PostAddComponent} from './components/post-add/post-add.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {PostService} from './services/post.service';
import {PostListComponent} from './components/post-list/post-list.component';
import {
  MdButtonModule,
  MdButtonToggleModule,
  MdCardModule,
  MdCheckboxModule,
  MdDialogConfig,
  MdDialogModule,
  MdExpansionModule,
  MdIconModule,
  MdInputModule,
  MdMenuModule,
  MdRadioModule,
  MdSidenavModule,
  MdSnackBarModule,
  MdToolbarModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoginComponent} from './components/login/login.component';
import {AuthGuard} from './guards/auth.guard';
import {AuthenticationService} from './services/authentication.service';
import {HttpModule} from '@angular/http';
import {RegistrationComponent} from './components/registration/registration.component';
import {FileUploadService} from './services/file-upload.service';
import {SmallPostComponent} from './components/small-post/small-post.component';
import {UserService} from './services/user.service';
import {MainViewComponent} from './components/main-view/main-view.component';
import {ToolbarComponent} from './components/toolbar/toolbar.component';
import {RouteGuard} from './guards/route.guard';
import {ColorChooserComponent} from './components/color-chooser/color-chooser.component';
import {UIService} from './services/ui.service';
import {BoardListComponent} from './components/board-list/board-list.component';
import {BoardAddComponent} from './components/board-add/board-add.component';
import {BoardService} from './services/board.service';

const appRoutes: Routes =
  [
    {path: 'main', component: MainViewComponent, canActivate: [RouteGuard]},
    {path: 'post_list', component: PostListComponent, canActivate: [AuthGuard]},
    {path: '', redirectTo: 'post_list', pathMatch: 'full'},
    {path: '**', component: PageNotFoundComponent}
  ];

@NgModule({
  declarations: [
    AppComponent,
    PostAddComponent,
    PageNotFoundComponent,
    PostListComponent,
    LoginComponent,
    RegistrationComponent,
    SmallPostComponent,
    MainViewComponent,
    ToolbarComponent,
    ColorChooserComponent,
    BoardListComponent,
    BoardAddComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    MdDialogModule,
    BrowserAnimationsModule,
    MdCardModule,
    MdButtonModule,
    MdIconModule,
    MdSidenavModule,
    MdToolbarModule,
    MdMenuModule,
    HttpModule,
    MdInputModule,
    MdCheckboxModule,
    MdSnackBarModule,
    MdButtonToggleModule,
    MdRadioModule,
    MdExpansionModule
  ],
  entryComponents: [
    LoginComponent,
    RegistrationComponent
  ],
  providers: [
    PostService,
    BoardService,
    MdDialogConfig,
    AuthGuard,
    AuthenticationService,
    FileUploadService,
    UserService,
    RouteGuard,
    UIService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {

}
