export class Urls {
  public static readonly backendUrl = 'http://localhost:29270';
  public static readonly webApiUrl = `${Urls.backendUrl}/api`;

  public static readonly postUrl = `${Urls.webApiUrl}/post`;
  public static readonly postWithLimitUrl = `${Urls.webApiUrl}/post?limit`;
  public static readonly reactUrl = `${Urls.postUrl}/react`;
  public static readonly imagesUrl = `${Urls.backendUrl}/images`;
  public static readonly fileUploadUrl = `${Urls.webApiUrl}/fileUpload`;

  public static readonly boardUrl = `${Urls.webApiUrl}/board`;
  public static readonly boardWithLimitUrl = `${Urls.boardUrl}?limit`;

  public static readonly accountApiUrl = `${Urls.webApiUrl}/Account`;
  public static readonly userUrl = `${Urls.webApiUrl}/users`;
  public static readonly loginUrl = `${Urls.accountApiUrl}/sign-in`;
  public static readonly registerUrl = `${Urls.accountApiUrl}/register`;
  public static readonly getUserByIdUrl = `${Urls.userUrl}/getUserById`;
  public static readonly getUserByUsernameUrl = `${Urls.userUrl}/getUserByUsername?username=`;
}
