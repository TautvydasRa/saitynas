import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Urls} from '../recources/urls';
import {User} from '../models/user';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  static getUserAvatarUrl(bareAvatarUrl: string) {
    return `${Urls.imagesUrl}/${bareAvatarUrl}`;
  }

  // todo remove
  getUserById(userId: string): Observable<User> {
    return this.http.get(Urls.getUserByIdUrl + userId);
  }

  getUserByUsername(username: string): Observable<any> {
    return this.http.get(`${Urls.getUserByUsernameUrl}${username}`);
  }
}
