import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class UIService {
  private addPostEventHandler;
  private boardsEventHandler;
  private colorFilterEventHandler;

  constructor() {
    this.addPostEventHandler = new Subject();
    this.boardsEventHandler = new Subject();
    this.colorFilterEventHandler = new Subject();
  }

  invokeAddPostSideNav() {
    this.addPostEventHandler.next();
  }

  invokeBoardsSideNav() {
    this.boardsEventHandler.next();
  }

  invokeColorFilter(color) {
    this.colorFilterEventHandler.next(color);
  }

  addPostSideNavEvent(): Observable<any> {
    return this.addPostEventHandler.asObservable();
  }

  boardsSideNavEvent(): Observable<any> {
    return this.boardsEventHandler.asObservable();
  }

  colorFilterEvent(): Observable<any> {
    return this.colorFilterEventHandler.asObservable();
  }
}
