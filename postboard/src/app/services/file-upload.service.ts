import {Injectable} from '@angular/core';
import {Urls} from '../recources/urls';
import {HttpClient, HttpEvent, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class FileUploadService {
  constructor(private http: HttpClient) {
  }

  static getImageUrl(bareImageUrl: string) {
    return `${Urls.imagesUrl}/${bareImageUrl}`;
  }

  uploadFile(file: File): Observable<HttpEvent<any>> {
    const formData = new FormData();
    formData.append('files', file);

    const req = new HttpRequest('POST', Urls.fileUploadUrl, formData, {reportProgress: true});
    return this.http.request(req);
  }
}
