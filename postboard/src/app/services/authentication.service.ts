import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';
import {Urls} from '../recources/urls';

@Injectable()
export class AuthenticationService {
  public token: string;

  constructor(private http: Http, private router: Router) {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  login(username: string, password: string): Observable<boolean> {
    const body = JSON.stringify({username: username, password: password});
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({headers: headers});
    return this.http.post(Urls.loginUrl, body, options)
      .map((response: Response) => {
        const token = response.json() && response.json().value.id_token;
        if (token) {
          this.token = token;
          localStorage.setItem('currentUser', JSON.stringify({
            username: username, token: token
          }));
          this.router.navigate(['/post_list']);
          return true;
        }
        return false;
      });
  }

  register(username: string, password: string): Observable<boolean> {
    const body = JSON.stringify({username: username, password: password});
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({headers: headers});
    return this.http.post(Urls.registerUrl, body, options)
      .map((response: Response) => {
        const token = response.json().value && response.json().value.id_token;
        if (token) {
          this.token = token;
          localStorage.setItem('currentUser', JSON.stringify({
            username: username, token: token
          }));
          this.router.navigate(['/post_list']);
          return true;
        }
        return false;
      });
  }

  logout(): void {
    this.token = null;
    localStorage.removeItem('currentUser');
    this.router.navigate(['/main']);
  }
}
