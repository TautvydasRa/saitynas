import {Injectable} from '@angular/core';
import {Post} from '../models/post';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Urls} from '../recources/urls';
import {Reaction} from '../models/reaction';

@Injectable()
export class PostService {
  constructor(private http: HttpClient) {
  }

  addPost(post: Post): Observable<Post> {
    const body = JSON.stringify(post);
    console.log('Creating new post: ' + body);
    return this.http.post(Urls.postUrl, body, PostService.RequestOptions);
  }

  updatePost(post: Post): Observable<Post> {
    const body = JSON.stringify(post);
    console.log('Updating post: ' + body);
    return this.http.put(`${Urls.postUrl}`, body, PostService.RequestOptions);
  }

  getPosts(limit: number): Observable<Post[]> {
    return this.http.get(`${Urls.postWithLimitUrl}=${limit}`);
  }

  getPost(id: number): Observable<Post> {
    return this.http.get(`${Urls.postUrl}/${id}`);
  }

  deletePost(postId: number): Observable<Post> {
    console.log(`${Urls.postUrl}/${postId}`);
    return this.http.delete(`${Urls.postUrl}/${postId}`);
  }

  react(reaction: Reaction, postID: number): Observable<any> {
    const body = JSON.stringify(reaction);
    return this.http.put(`${Urls.reactUrl}/${postID}`, body, PostService.RequestOptions);
  }

  removeReaction(username: string, postID: number): Observable<number> {
    return this.http.delete(`${Urls.postUrl}/${username},${postID}`);
  }

  private static get RequestOptions() {
    return {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    };
  }
}
