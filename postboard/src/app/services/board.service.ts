import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Urls} from '../recources/urls';
import {Board} from '../models/board';
import {Observable} from 'rxjs/Observable';
import {Post} from '../models/post';
import {observable} from 'rxjs/symbol/observable';

@Injectable()
export class BoardService {
  constructor(private httpClient: HttpClient) {
  }

  addBoard(board: Board): Observable<Board> {
    const body = JSON.stringify(board);
    console.log('Adding new board: ' + body);
    return this.httpClient.post(Urls.boardUrl, body, BoardService.RequestOptions);
  }

  editBoard(board: Board): Observable<Board> {
    const body = JSON.stringify(board);
    return this.httpClient.put(`${Urls.boardUrl}`, body, BoardService.RequestOptions);
  }

  removeBoard(boardId: number): Observable<Board> {
    return this.httpClient.delete(`${Urls.boardUrl}/${boardId}`);
  }

  getBoard(boardId: number): Observable<Board> {
    return this.httpClient.get(`${Urls.boardUrl}/${boardId}`);
  }

  getBoards(limit: number): Observable<Board[]> {
    return this.httpClient.get(`${Urls.boardWithLimitUrl}=${limit}`);
  }

  private static get RequestOptions() {
    return {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    };
  }
}
